plugins {
    id("org.jetbrains.intellij") version "1.13.3"
    java
    kotlin("jvm") version "1.8.21"
}

group = "com.github.caijh.plugin"
version = "2.0.7"
val junitVersion = "5.8.2"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("org.junit.jupiter:junit-jupiter:$junitVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

// See https://github.com/JetBrains/gradle-intellij-plugin/
intellij {
    version.set("2022.2.1")
    updateSinceUntilBuild.set(false)
}
tasks {
    patchPluginXml {
        changeNotes.set(
            """
            <b>New features</b>
            <ul>
                <li>Fix Compatible, 3 usages of scheduled for removal API.</li>
            </ul>
            <br/>        """.trimIndent()
        )
        sinceBuild.set("222")
    }

    runPluginVerifier {
        ideVersions.set(listOf("2022.2.1", "2023.1"))
    }

    publishPlugin {
        token.set("")
    }
}
tasks.getByName<Test>("test") {
    useJUnitPlatform()
}
