package com.github.caijh.plugin.intelij.background.image

import com.github.caijh.plugin.intelij.background.image.ui.Settings
import com.intellij.ide.util.PropertiesComponent
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.project.ProjectManager

abstract class AbstractBackgroundImageAction : AnAction() {
    protected var scheduler = Scheduler
    protected var imagesHandler: ImagesHandler = ImagesHandler

    protected fun perform() {
        try {
            val task = RandomBackgroundTask(PropertiesComponent.getInstance(), imagesHandler)
            val defaultProject = ProjectManager.getInstance().defaultProject
            WriteCommandAction.runWriteCommandAction(defaultProject, task)
        } catch (e: Exception) {
            NotificationCenter.error(e.message, e)
        }
    }

    protected fun getTimeExecution(): Int {
        try {
            val timeExecution = PropertiesComponent.getInstance().getValue(Settings.TIME_EXECUTION)
            if (!Strings.isEmpty(timeExecution)) {
                return timeExecution!!.toInt()
            }
        } catch (e: NumberFormatException) {
            NotificationCenter.notify("Please, specify a valid integer number as execution time.")
        }
        return Settings.DEFAULT_TIME_EXECUTION
    }

}
