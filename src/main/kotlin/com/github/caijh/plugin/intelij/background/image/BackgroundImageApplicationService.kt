package com.github.caijh.plugin.intelij.background.image

import com.intellij.openapi.components.Service

@Service
final class BackgroundImageApplicationService {

    @Volatile
    var hasRunActivity = false
}