package com.github.caijh.plugin.intelij.background.image

import com.intellij.ide.util.PropertiesComponent
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.wm.impl.IdeBackgroundUtil

class ClearBackgroundImageAction : AbstractBackgroundImageAction() {

    override fun actionPerformed(e: AnActionEvent) {
        scheduler.cancel()
        val prop = PropertiesComponent.getInstance()
        prop.setValue(IdeBackgroundUtil.EDITOR_PROP, null)
        prop.setValue(IdeBackgroundUtil.FRAME_PROP, null)
        prop.setValue(IdeBackgroundUtil.TARGET_PROP, null)
        IdeBackgroundUtil.repaintAllWindows()
    }
}