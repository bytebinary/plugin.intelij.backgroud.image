package com.github.caijh.plugin.intelij.background.image

object Constants {
    const val DISPLAY_NAME = "Background Image"
    const val RANDOM_BACKGROUND_IMAGE = "Random Background Image"
    const val CLEAR_BACKGROUND_IMAGE = "Clear Background Image"
    const val ORDER_BY_FILE_NAME = "file name"
    const val ORDER_BY_FILE_CREATED_DATE = "file last modified date"
}
