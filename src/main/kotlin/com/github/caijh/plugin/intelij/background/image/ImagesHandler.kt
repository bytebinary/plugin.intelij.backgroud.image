package com.github.caijh.plugin.intelij.background.image

import org.apache.commons.io.comparator.NameFileComparator
import org.apache.commons.lang.StringUtils
import org.jetbrains.annotations.NonNls
import java.io.File
import java.util.*
import java.util.concurrent.ThreadLocalRandom

object ImagesHandler {

    @Volatile
    private var nextInt = -1
    private var imageSize = 0

    private val comparatorMap: MutableMap<String, Comparator<File>> = mutableMapOf()
    private val defaultComparator = NameFileComparator.NAME_COMPARATOR

    init {
        comparatorMap[Constants.ORDER_BY_FILE_NAME] = defaultComparator
        comparatorMap[Constants.ORDER_BY_FILE_CREATED_DATE] = Comparator.comparingLong { it.lastModified() }
    }

    fun getRandomImage(folder: String, orderBy: @NonNls String): String {
        if (Strings.isEmpty(folder)) {
            return Strings.EMPTY
        }
        val images = collectImages(folder, orderBy)
        if (imageSize != images.size || nextInt == -1) {
            imageSize = images.size
            nextInt = ThreadLocalRandom.current().nextInt(images.size)
        } else {
            nextInt = (nextInt + 1) % imageSize
        }

        return Optional.ofNullable(images[nextInt]).orElse(StringUtils.EMPTY)
    }

    fun resetNextInt() {
        this.nextInt = -1
    }

    private fun collectImages(rootFolder: String, orderBy: @NonNls String): List<String> {
        val folder = File(rootFolder)
        if (!folder.exists()) {
            return emptyList()
        }
        val comparator = comparatorMap[orderBy] ?: defaultComparator
        return folder.listFiles { file -> file.isFile && (file.extension == "png" || file.extension == "jpg" || file.extension == "jpeg") }
            ?.sortedWith(comparator)
            ?.map { it.absolutePath }
            ?: emptyList()
    }

}
