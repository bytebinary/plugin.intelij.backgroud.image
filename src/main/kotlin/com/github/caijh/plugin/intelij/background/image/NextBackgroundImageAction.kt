package com.github.caijh.plugin.intelij.background.image

import com.intellij.openapi.actionSystem.AnActionEvent
import java.util.concurrent.TimeUnit

class NextBackgroundImageAction : AbstractBackgroundImageAction() {

    override fun actionPerformed(e: AnActionEvent) {
        scheduler.scheduleWithFixedDelay(
            { perform() }, 0, getTimeExecution().toLong(), TimeUnit.SECONDS
        )
    }

}