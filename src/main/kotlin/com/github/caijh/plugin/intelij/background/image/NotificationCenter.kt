package com.github.caijh.plugin.intelij.background.image

import com.intellij.notification.NotificationGroupManager
import com.intellij.notification.NotificationType
import com.intellij.notification.Notifications
import com.intellij.openapi.diagnostic.Logger

object NotificationCenter {
    private val LOG = Logger.getInstance(
        NotificationCenter::class.java
    )
    private const val MESSAGE_PREFIX = "[Random Background]"

    @JvmStatic
    fun notify(message: String?) {
        val n = NotificationGroupManager.getInstance()
            .getNotificationGroup("com.github.caijh.plugin.intelij.background.image.NotificationCenter")
            .createNotification(
                "Notice", String.format("%s - %s", MESSAGE_PREFIX, message), NotificationType.INFORMATION
            )
        Notifications.Bus.notify(n)
    }

    @JvmStatic
    fun error(message: String?, t: Throwable?) {
        LOG.error(message, t)
    }

}
