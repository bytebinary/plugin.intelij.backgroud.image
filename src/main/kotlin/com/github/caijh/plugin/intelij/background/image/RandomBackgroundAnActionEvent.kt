package com.github.caijh.plugin.intelij.background.image

import com.github.caijh.plugin.intelij.background.image.Constants.RANDOM_BACKGROUND_IMAGE
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.DataContext
import com.intellij.openapi.actionSystem.Presentation

class RandomBackgroundAnActionEvent : AnActionEvent(
    null,
    DataContext.EMPTY_CONTEXT,
    Constants.DISPLAY_NAME,
    Presentation(RANDOM_BACKGROUND_IMAGE),
    ActionManager.getInstance(),
    0
)