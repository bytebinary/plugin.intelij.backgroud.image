package com.github.caijh.plugin.intelij.background.image

import com.github.caijh.plugin.intelij.background.image.NotificationCenter.notify
import com.github.caijh.plugin.intelij.background.image.ui.Settings
import com.intellij.ide.util.PropertiesComponent
import com.intellij.openapi.wm.impl.IdeBackgroundUtil
import java.nio.file.Files
import java.nio.file.Paths

internal class RandomBackgroundTask(
    private val prop: PropertiesComponent,
    private val imagesHandler: ImagesHandler
) : Runnable {
    override fun run() {
        if (!prop.getBoolean(Settings.DISABLED)) {
            val folder = prop.getValue(Settings.FOLDER) ?: ""
            if (Strings.isEmpty(folder)) {
                notify("Image folder not set")
                return
            }
            if (Files.notExists(Paths.get(folder))) {
                notify("Image folder doesn't exists")
                return
            }
            val orderBy = prop.getValue(Settings.ORDER_BY) ?: Constants.ORDER_BY_FILE_NAME
            val image = imagesHandler.getRandomImage(folder, orderBy)
            if (Strings.isEmpty(image)) {
                notify("No image found")
                return
            }
            val storedOpacity = prop.getValue(Settings.OPACITY)
            val ideaBackgroundImage = String.format("%s,%s", image, storedOpacity)
            prop.setValue(IdeBackgroundUtil.FRAME_PROP, null)
            prop.setValue(IdeBackgroundUtil.EDITOR_PROP, ideaBackgroundImage)
            IdeBackgroundUtil.repaintAllWindows()
        }
    }
}
