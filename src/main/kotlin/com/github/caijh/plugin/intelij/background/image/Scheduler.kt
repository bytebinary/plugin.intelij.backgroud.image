package com.github.caijh.plugin.intelij.background.image

import java.util.concurrent.*

object Scheduler {
    private val executorService: ScheduledExecutorService = Executors.newScheduledThreadPool(1, object : ThreadFactory {
        private val defaultFactory = Executors.defaultThreadFactory()
        override fun newThread(r: Runnable): Thread {
            val thread = defaultFactory.newThread(r)
            thread.isDaemon = true
            return thread
        }
    })

    @Volatile
    private var future: ScheduledFuture<*>? = null

    fun scheduleWithFixedDelay(runnable: Runnable, initialDelay: Long, period: Long, unit: TimeUnit) {
        cancel()
        future = executorService.scheduleWithFixedDelay(runnable, initialDelay, period, unit)
    }

    fun cancel() {
        if (future != null && !future!!.isCancelled) {
            future!!.cancel(true)
        }
    }
}