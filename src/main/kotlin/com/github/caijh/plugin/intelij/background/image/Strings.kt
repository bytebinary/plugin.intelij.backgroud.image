package com.github.caijh.plugin.intelij.background.image

object Strings {
    const val EMPTY: String = ""
    fun isEmpty(str: String?): Boolean {
        return str.isNullOrEmpty()
    }

    fun isNotEmpty(str: String?): Boolean {
        return isEmpty(str).not()
    }
}
