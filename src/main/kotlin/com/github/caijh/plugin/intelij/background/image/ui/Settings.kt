package com.github.caijh.plugin.intelij.background.image.ui

import com.github.caijh.plugin.intelij.background.image.*
import com.intellij.ide.plugins.IdeaPluginDescriptor
import com.intellij.ide.plugins.PluginInstaller
import com.intellij.ide.plugins.PluginStateListener
import com.intellij.ide.util.PropertiesComponent
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.components.service
import com.intellij.openapi.options.Configurable
import com.intellij.openapi.project.Project
import com.intellij.openapi.startup.StartupActivity
import org.apache.commons.lang.StringUtils
import org.jetbrains.annotations.Nls
import java.io.File
import java.util.*
import javax.swing.*

class Settings : Configurable, StartupActivity {
    private var imageFolder: JTextField? = null
    private var orderByOption: JComboBox<String>? = null
    private var rootPanel: JPanel? = null
    private var chooser: JButton? = null
    private var timeExecution: JTextField? = null
    private var opacity: JSlider? = null
    private var opacityLabel: JLabel? = null
    private var disabled: JCheckBox? = null

    override fun getDisplayName(): @Nls String {
        return Constants.DISPLAY_NAME
    }

    override fun createComponent(): JComponent? {
        chooser!!.addActionListener {
            val fc = JFileChooser()
            val current = imageFolder!!.text
            if (current.isNotEmpty()) {
                fc.currentDirectory = File(current)
            }
            fc.fileSelectionMode = JFileChooser.DIRECTORIES_ONLY
            fc.showOpenDialog(rootPanel)
            val file = fc.selectedFile
            val path = if (file == null) StringUtils.EMPTY else file.absolutePath
            imageFolder!!.text = path
        }
        opacityLabel?.text = opacity?.value.toString() + "%"
        opacity?.addChangeListener { opacityLabel?.text = opacity?.value.toString()  + "%"}
        return rootPanel
    }

    override fun isModified(): Boolean {
        val prop = PropertiesComponent.getInstance()
        val storedFolder = Optional.ofNullable(prop.getValue(FOLDER)).orElse(StringUtils.EMPTY)
        val storeOrderBy = Optional.ofNullable(prop.getValue(ORDER_BY)).orElse(Constants.ORDER_BY_FILE_NAME)
        val storedTimeExecution = Optional.ofNullable(prop.getValue(TIME_EXECUTION)).orElse(StringUtils.EMPTY)
        val storedOpacity = getStoredOpacity(prop)
        val storedDisabledOption = prop.getBoolean(DISABLED)
        val uiFolder = imageFolder!!.text
        val uiOrderBy = orderByOption!!.selectedItem
        val uiTimeExecution = timeExecution!!.text
        val uiOpacity = opacity!!.value
        val isDisabled = disabled!!.isSelected
        return (storedFolder != uiFolder
                || storeOrderBy != uiOrderBy
                || storedTimeExecution != uiTimeExecution
                || storedOpacity != uiOpacity || storedDisabledOption != isDisabled)
    }

    override fun runActivity(project: Project) {
        val prop = PropertiesComponent.getInstance()
        val applicationService = service<BackgroundImageApplicationService>()
        if (!prop.getBoolean(DISABLED) && !applicationService.hasRunActivity) {
            applicationService.hasRunActivity = true
            ActionManager.getInstance().getAction("randomBackgroundImage")
                .actionPerformed(RandomBackgroundAnActionEvent())
        }
        PluginInstaller.addStateListener(
            object : PluginStateListener {
                override fun install(ideaPluginDescriptor: IdeaPluginDescriptor) {
                    // do thing
                }

                override fun uninstall(ideaPluginDescriptor: IdeaPluginDescriptor) {
                    disable()
                }
            })
    }

    override fun apply() {
        val prop = PropertiesComponent.getInstance()
        prop.setValue(FOLDER, imageFolder!!.text)
        prop.setValue(ORDER_BY, orderByOption!!.selectedItem?.toString())
        prop.setValue(TIME_EXECUTION, timeExecution!!.text)
        prop.setValue(OPACITY, opacity!!.value.toString())
        prop.setValue(DISABLED, disabled!!.isSelected)
        if (disabled!!.isSelected) {
            disable()
        } else {
            enable()
        }
    }

    private fun enable() {
        val randomBackgroundImageAction =
            ActionManager.getInstance().getAction("randomBackgroundImage") as RandomBackgroundImageAction
        randomBackgroundImageAction.actionPerformed(RandomBackgroundAnActionEvent())
    }

    private fun disable() {
        val clearBackgroundImageAction =
            ActionManager.getInstance().getAction("clearBackgroundImage") as ClearBackgroundImageAction
        clearBackgroundImageAction.actionPerformed(ClearBackgroundAnActionEvent())
    }

    override fun reset() {
        val prop = PropertiesComponent.getInstance()
        val storedOpacity = getStoredOpacity(prop)
        opacity!!.value = storedOpacity
        imageFolder!!.text = prop.getValue(FOLDER)
        orderByOption!!.selectedItem = prop.getValue(ORDER_BY)
        timeExecution!!.text = prop.getValue(TIME_EXECUTION)
        disabled!!.isSelected = prop.getBoolean(DISABLED)
    }

    companion object {
        const val DEFAULT_TIME_EXECUTION = 300
        const val FOLDER = "BackgroundImagesFolder"
        const val ORDER_BY = "BackgroundImagesOrderBy"
        const val TIME_EXECUTION = "BackgroundImagesTimeExecution"
        const val OPACITY = "BackgroundImagesOpacity"
        const val DISABLED = "BackgroundImagesDisabled"
        fun getStoredOpacity(prop: PropertiesComponent): Int {
            return Optional.ofNullable(prop.getValue(OPACITY)).orElse("30").toInt()
        }
    }
}
